'''
1 | 2 | 3
---------
4 | 5 | 6
---------
7 | 8 | 9

player X modificat branch1
player O
'''
from typing import Sequence

class game():
    table = {1: '   ', 2: '   ', 3: '   ', 4: '   ', 5: '   ', 6: '   ', 7: '   ', 8: '   ', 9: '   '}
    player = 'X'
    i=1
    def update_table(self):
        b=1
        while b <= 9:
            if b==9:
                print(self.table[b])
            else:
                print(self.table[b] + '|', end = "")
            b+=1
            if b==3 or b==6:
                print(self.table[b] + '\n' + '---+---+---')
                b+=1

    def play(self):
        while self.i <= 9 :
            print ("\nIt turn for " + self.player + " to fill a square")
            print ("What is the next move ? (1-9)")
            
            try:
                square_no = int(input())
                if square_no not in range(1,10):
                    print("This is not a valid move")
                else:
                    if self.table[square_no] == '   ':
                        self.table[square_no] = ' ' + self.player + ' '
                        self.i +=1
                        self.winner()
                        self.player_turn()
                    else:
                        print("That place has been filled before")

                self.update_table()

            except ValueError:
                print("This is not an int")
    
    def player_turn(self):
        if self.player =='X':
            self.player = 'O'
        else:
            self.player = 'X'

    def winner(self):
        if self.i>=5:
            if self.table[1] == self.table[2] == self.table[3] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #row1
            elif self.table[4] == self.table[5] == self.table[6] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #row2
            elif self.table[7] == self.table[8] == self.table[9] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #row3

            elif self.table[1] == self.table[4] == self.table[7] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #column1
            elif self.table[2] == self.table[5] == self.table[8] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #column2
            elif self.table[3] == self.table[6] == self.table[9] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #column3

            elif self.table[1] == self.table[5] == self.table[9] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #diag1
            elif self.table[3] == self.table[5] == self.table[7] != '   ': self.update_table(), print("Game over. ", self.player, " Won"), exit() #diag2

            elif self.i==10: self.update_table(), print("Game over. It's a tie !"), exit()
           
g=game()
g.update_table()
g.play()
