class game:
    def __init__(self, number_of_columns):
        self.number_of_columns = number_of_columns
        self.matrix = [['   ' for x in range(self.number_of_columns)] for y in range(self.number_of_columns)]
        self.z=1
        self.player = 'O'
        #print(self.matrix)
    
    def update_table(self):
        self.table = []
        counter = 0
        n=0
        for i in range (self.number_of_columns):
            n+=1
            for element in self.matrix[i]:
                self.table.append(element)
                print(self.table[counter], end='')
                counter+=1
                if counter < (self.number_of_columns*n):
                    print("|", end='')
            print('\n',end = '')
            if i < self.number_of_columns-1:
                #print ('---+---+---')
                for a in range (self.number_of_columns):
                    for b in range(3):
                        print('-', end='') #print ---
                    if a < self.number_of_columns-1:
                       print('+', end='') #print + after every ---   -> so it goes like ---+
            print('\n', end = '')
    
    def fill_in_matrix(self):
        print ("\nIt turn for " + self.player + " to fill a square")
        print ("What is the next move ? (1-9)")
        try:
            square_no = int(input())
            if square_no not in range(1,10):
                print("This is not a valid move")
            else:
                counter_square=1
                for m in range(self.number_of_columns): #go through all elements in matrix
                    for element in range(len(self.matrix[m])):
                        if counter_square == square_no: #check if the counter is the same as the imput from player
                            if self.matrix[m][element] == '   ': #check if the value in matrix is empty
                                self.matrix[m][element] = ' ' + self.player + ' ' #fill the new value in matrix
                                #self.player_turn()
                                #mai ramane aici sa fac validarea castigatorului
                            else:
                                print("That place has been filled before")
                            #print(self.matrix)
                        counter_square+=1
        except:
            print("This is not an INT")

    def player_turn(self):
        if self.player =='X':
            self.player = 'O'
        else:
            self.player = 'X'
    
    def play(self):
        self.fill_in_matrix()
        self.update_table()
        
    def validate_game(self):
        #print ("aaa")
        for p in range(3):
            if self.matrix[p][0] == self.matrix[p][1] == self.matrix[p][2] != '   ': self.we_have_a_winner(); return True               # 3 same in a row
            elif self.matrix[0][p] == self.matrix[1][p] == self.matrix[2][p] != '   ': self.we_have_a_winner(); return True             # 3 same in a column
            try: #here is a try catch due to case matrix[1][3]/ matrix[2][4] etc.
                if self.matrix[p][p] == self.matrix[p+1][p+1] == self.matrix[p+2][p+2] != '   ': self.we_have_a_winner(); return True   # 3 same in first diag
                if self.matrix[p][p+2] == self.matrix[p+1][p+1] == self.matrix[p+2][p] != '   ': self.we_have_a_winner(); return True   # 3 same in second diag
            except:
                pass
            if not any('   ' in x for x in self.matrix): print("Game over. It's a Tie"); return True # the board is full
        self.player_turn()

    def we_have_a_winner(self):
        self.update_table()
        print("Game over. ", self.player, " Won")
        

#Here you will fill the number of elements per row
#Matrix will be rows x columns
g = game(3)
g.update_table()

while g.validate_game() != True:
    g.play()