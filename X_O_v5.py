#####
# Something New
#####

class game:
    def __init__(self, number_of_columns):
        self.number_of_columns = number_of_columns
        self.player_turn = 0
        self.player_mapped = {0: 'X', 1: 'O', 2: ' '}
        
        self.matrix = [[' ' + self.player_mapped[2] + ' ' for x in range(self.number_of_columns)] for y in range(self.number_of_columns)]
        self.first_diag = ['   ' for x in range(self.number_of_columns)]
        self.second_diag = ['   ' for x in range(self.number_of_columns)]
    
    def update_table(self):
        for idm, valm in enumerate(self.matrix):
            for id, val in enumerate(valm):
                if id == (len(valm) - 1): #end of line
                    print(val, end='\n')    
                else:
                    print(val, end='|') 
            if idm != (len(valm) - 1):
                for i in range(len(valm)):
                    if i == (len(valm) - 1):
                        print("---",end="\n") #end of line
                    else:
                        print("---+", end="")
    
    def fill_in_matrix(self):
        print ("\nIt turn for " + self.player_mapped[self.player_turn] + " to fill a square")
        print ("What is the next move ? (1-9)")
        try:
            square_no = int(input())
            if square_no not in range(1,10):
                print("This is not a valid move")
            else:
                counter_square=1
                for m in range(self.number_of_columns): #go through all elements in matrix
                    for element in range(len(self.matrix[m])):
                        if counter_square == square_no: #check if the counter is the same as the imput from player
                            if self.matrix[m][element] == ' ' + self.player_mapped[2] + ' ': #check if the value in matrix is empty
                                self.matrix[m][element] = ' ' + self.player_mapped[self.player_turn] + ' ' #fill the new value in matrix 
                            else:
                                print("That place has been filled before")
                        counter_square+=1
        except:
            print("This is not an INT")
    
    def play(self):
        self.fill_in_matrix()
        self.update_table()
   
    def validate_game(self):
        for id_matrix, val_matrix in enumerate(self.matrix):
            for id_from_array, val_from_array in enumerate(val_matrix):
                column =  [val[id_matrix] for val in self.matrix]
                #Condition for row
                if (all(elem == val_matrix[0] != '   ' for elem in val_matrix)) == True : self.we_have_a_winner(); return True

                #Condition for column
                if (all(elem == column[0] != '   ' for elem in column)) == True: self.we_have_a_winner(); return True

                #Condition for first diag
                if id_matrix == id_from_array:
                    #print("Val_From_Array:  ", val_matrix[id_from_array])
                    self.first_diag[id_from_array] = val_matrix[id_from_array]
                    if (all(elem == self.first_diag[0] != '   ' for elem in self.first_diag)) == True: self.we_have_a_winner(); return True
                
                #Condition for second diag
                if (id_matrix + id_from_array) == (self.number_of_columns - 1):
                    self.second_diag[id_matrix] = val_matrix[id_from_array]
                
                if (all(elem == self.second_diag[0] != '   ' for elem in self.second_diag)) == True: self.we_have_a_winner(); return True

        if self.player_turn == 0:
            self.player_turn = 1
        else:
            self.player_turn = 0


    def we_have_a_winner(self):
        print("Game over. ", self.player_mapped[self.player_turn], " Won")

#Here you will fill the number of elements per row
#Matrix will be rows x columns
g = game(3)
g.update_table()

while g.validate_game() != True:
    g.play()
